Source: rust-ureq
Section: rust
Priority: optional
Build-Depends:
 ca-certificates <!nocheck>,
 debhelper-compat (= 13),
 dh-sequence-rust,
 librust-base64-0.22+default-dev,
 librust-brotli-decompressor-4+default-dev,
 librust-chunked-transfer-1+default-dev,
 librust-cookie-0.18-dev,
 librust-cookie-store-0.21+preserve-order-dev,
 librust-encoding-rs-0.8+default-dev,
 librust-env-logger-0.11+humantime-dev,
 librust-flate2-1+default-dev (>= 1.0.22),
 librust-http-0.2+default-dev,
 librust-http-1+default-dev,
 librust-log-0.4+default-dev,
 librust-native-tls-0.2+default-dev,
 librust-once-cell-1+default-dev,
 librust-rustls-0.21+default-dev,
 librust-rustls-native-certs-dev (<< 0.8),
 librust-rustls-pemfile-1+default-dev,
 librust-rustls-webpki-0.101+default-dev,
 librust-serde-1+default-dev,
 librust-serde-1+derive-dev,
 librust-serde-json-1+default-dev (>= 1.0.97),
 librust-socks-0.3+default-dev,
 librust-url-2+default-dev,
Maintainer: Jonas Smedegaard <dr@jones.dk>
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/debian/rust-ureq.git
Vcs-Browser: https://salsa.debian.org/debian/rust-ureq
Homepage: https://github.com/algesten/ureq
Rules-Requires-Root: no

Package: librust-ureq-dev
Architecture: all
Multi-Arch: foreign
Depends:
 librust-base64-0.22+default-dev,
 librust-brotli-decompressor-4+default-dev,
 librust-chunked-transfer-1+default-dev,
 librust-cookie-0.18-dev,
 librust-cookie-store-0.21+preserve-order-dev,
 librust-encoding-rs-0.8+default-dev,
 librust-flate2-1+default-dev (>= 1.0.22),
 librust-http-0.2+default-dev,
 librust-http-1+default-dev,
 librust-log-0.4+default-dev,
 librust-native-tls-0.2+default-dev,
 librust-once-cell-1+default-dev,
 librust-rustls-0.21+default-dev,
 librust-rustls-native-certs-dev (<< 0.8),
 librust-rustls-webpki-0.101+default-dev,
 librust-serde-1+default-dev,
 librust-serde-json-1+default-dev (>= 1.0.97),
 librust-socks-0.3+default-dev,
 librust-url-2+default-dev,
 ${misc:Depends},
Recommends:
 ca-certificates,
Provides:
 librust-ureq-2+brotli-dev (= ${binary:Version}),
 librust-ureq-2+charset-dev (= ${binary:Version}),
 librust-ureq-2+cookies-dev (= ${binary:Version}),
 librust-ureq-2+default-dev (= ${binary:Version}),
 librust-ureq-2+gzip-dev (= ${binary:Version}),
 librust-ureq-2+http-crate-dev (= ${binary:Version}),
 librust-ureq-2+http-interop-dev (= ${binary:Version}),
 librust-ureq-2+json-dev (= ${binary:Version}),
 librust-ureq-2+native-certs-dev (= ${binary:Version}),
 librust-ureq-2+native-tls-dev (= ${binary:Version}),
 librust-ureq-2+proxy-from-env-dev (= ${binary:Version}),
 librust-ureq-2+socks-proxy-dev (= ${binary:Version}),
 librust-ureq-2+tls-dev (= ${binary:Version}),
 librust-ureq-2-dev (= ${binary:Version}),
 librust-ureq-2.12-dev (= ${binary:Version}),
 librust-ureq-2.12.1-dev (= ${binary:Version}),
Description: simple and safe HTTP client - Rust source code
 Ureq is a simple, safe HTTP client.
 .
 Ureq's first priority is being easy for you to use.
 It's great for anyone who wants a low-overhead HTTP client
 that just gets the job done.
 Works very well with HTTP APIs.
 Its features include cookies, JSON, HTTP proxies, HTTPS,
 and charset decoding.
 .
 Ureq is in pure Rust for safety and ease of understanding.
 It avoids using "unsafe" directly.
 It uses blocking I/O instead of async I/O,
 because that keeps the API simple and keeps dependencies to a minimum.
 For TLS, ureq uses rustls or native-tls.
 .
 This package contains the source for the Rust ureq crate,
 for use with cargo.
